EXE_NAME = dpcli

SDK_INCLUDES = ../../../Include
OUT_DIR ?= .

CCFLAGS = -g -Wall -I$(SDK_INCLUDES) $(CFLAGS)

ifneq ($(findstring CYGWIN, $(shell uname)),)
	#under Cygwin symbolic links cannot be dereferenced by the toolchain, need to specify the actual library for linking
	LDFLAGS = -lm -lc $(shell ls $(LIB_OUT_DIR)/libdpfpdd.so.?.?.?) $(shell ls $(LIB_OUT_DIR)/libdpfj.so.?.?.?) $(CFLAGS)
else
	LDFLAGS = -lm -lc -ldpfpdd -ldpfj $(CFLAGS)
endif

OBJS = dpcli.o helpers.o selection.o verification.o enrollment.o identify.o

all: $(OBJS)
	mkdir -p $(OUT_DIR)
	$(CC) $(OBJS) $(LDFLAGS) -o $(OUT_DIR)/$(EXE_NAME)

clean:
	rm -f $(OUT_DIR)/$(EXE_NAME) *.o *~

%.o: %.c
	$(CC) $(CCFLAGS) -c $< -o $@

.PHONY: install
