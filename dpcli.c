/* 
 * Copyright (C) 2011, Digital Persona, Inc.
 *
 * This file is a part of sample code for the UareU SDK 2.x.
 */


#include "helpers.h"
#include "selection.h"
#include "verification.h"
#include "enrollment.h"
#include "identify.h"

#include <dpfpdd.h>

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// main
// 

/**
* command: dplci <actioncode> <userid>
* 
*	eg. enrolment (actioncode=101)
*          run ./dpcli 101 jammer
*
*  actioncodes:
*      101 - enrolment
*      102 - verification
*      
*/

int main(int argc, char** argv){
	//block all signals, SIGINT will be unblocked and handled in CaptureFinger()
	sigset_t sigmask;
	sigfillset(&sigmask);
	sigprocmask(SIG_BLOCK, &sigmask, NULL);
	
	//initialize capture library
	int result = dpfpdd_init();
		
	if (argc < 2) {
		printf("Please check parameters.\n");
		printf("usage: dpcli <actioncode> <username>\n");
		printf("where <actioncode>:\n");
		printf("  101 - Enrolment\n");
        printf("  102 - Verification\n");
		return 1;
	}

	DPFPDD_DEV hReader = NULL; //handle of the selected reader
	char szReader[MAX_DEVICE_NAME_LENGTH]; //name of the selected reader

	if (DPFPDD_SUCCESS != result) {
		logInfo("Error initializing reader. Already Open.");
		return 99;
	} else {
		
		//close reader if opened
		if(NULL != hReader){
			result = dpfpdd_close(hReader);
			if(DPFPDD_SUCCESS != result) print_error("dpfpdd_close()", result);
			hReader = NULL;
		}
		//open new reader
		hReader = SelectAndOpenReader(szReader, sizeof(szReader));
		if (DPFPDD_SUCCESS != result) {
			logInfo("Error initializing reader. Already Open.");
			return 99;
		}
		if(strcmp(argv[1],"101")==0 && argv[2] != NULL){ 
			//printf("enrol %s\n", argv[2]);
			if(NULL == hReader){
				logInfo("\nReader is not selected!");
			}
			else{
				Enrollment(hReader, argv[2]);
			}
		} else if(strcmp(argv[1],"102")==0 && argv[2] != NULL) {
			//printf("verify %s\n", argv[2]);
			if(NULL == hReader){
				logInfo("\nReader is not selected!");
			}
			else{
				Verification(hReader, argv[2]);
			}
		} else if(strcmp(argv[1],"103")==0) {
			//printf("verify %s\n", argv[2]);
			if(NULL == hReader){
				logInfo("\nReader is not selected!");
			}
			else{
				Identify(hReader);
			}
		} else {
			logInfo("Unknown action code or parameter");
		}
	}
	//close reader
	if(NULL != hReader){
		result = dpfpdd_close(hReader);
		if(DPFPDD_SUCCESS != result) print_error("dpfpdd_close()", result);
		hReader = NULL;
	}
	dpfpdd_exit(); 
	return 0;
}
