/* 
 * Copyright (C) 2011, Digital Persona, Inc.
 *
 * This file is a part of sample code for the UareU SDK 2.x.
 */

#include "verification.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "helpers.h"

#include <dpfj.h>

void Verification(DPFPDD_DEV hReader, char* filename){
	unsigned char* pFeatures1 = NULL;
	unsigned int nFeatures1Size = 0;
	unsigned char* pFeatures2 = NULL;
	unsigned int nFeatures2Size = 0;
	int bStop = 0;

	//get reference fingerprint from file
	FILE* f = fopen(filename, "r");

	if (f == NULL) {
		logInfo("Template file not found.");
		bStop = 1;
	}

	while(!bStop){
		logInfo("Ready for verification");
		
		//capture first fingerprint
		int result = CaptureFinger("any finger", hReader, DPFJ_FMD_ISO_19794_2_2005, &pFeatures1, &nFeatures1Size);


		// Determine file size
		fseek(f, 0, SEEK_END);
		nFeatures2Size = ftell(f);
		rewind(f);
		pFeatures2 = (unsigned char*)malloc(nFeatures2Size);
		fread(pFeatures2, sizeof(char), nFeatures2Size, f);

		if(0 == result){
			//run comparison
			unsigned int falsematch_rate = 0;
			result = dpfj_compare(DPFJ_FMD_ISO_19794_2_2005, pFeatures1, nFeatures1Size, 0,
				DPFJ_FMD_ISO_19794_2_2005, pFeatures2, nFeatures2Size, 0, &falsematch_rate);
			
			if(DPFJ_SUCCESS == result){
				const unsigned int target_falsematch_rate = DPFJ_PROBABILITY_ONE / 100000; //target rate is 0.00001
				if(falsematch_rate < target_falsematch_rate){
					logInfo("Fingerprints matched.");
					//printf("dissimilarity score: 0x%x.\n", falsematch_rate);
					//printf("false match rate: %e.\n\n\n", (double)(falsematch_rate / DPFJ_PROBABILITY_ONE));
					bStop = 1;
				}
				else{
					logInfo("Fingerprints did not match.");
					bStop = 2;
				}
			}
			else print_error("dpfjmx_compare()", result);

		}
		else bStop = 1;
		
		//release memory
		if(NULL != pFeatures1) free(pFeatures1);
		pFeatures1 = NULL;
		nFeatures1Size = 0;
		if(NULL != pFeatures2) free(pFeatures2);
		pFeatures2 = NULL;
		nFeatures2Size = 0;
	}
}


