/* 
 * Copyright (C) 2011, Digital Persona, Inc.
 *
 * 	Modified file to select and automatically open first available reader
 *  for used with customized application
 */

#include "selection.h" 
#include "menu.h"
#include "helpers.h"

#include <dpfpdd.h>

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>


DPFPDD_DEV SelectAndOpenReader(char* szReader, size_t nReaderLen){
	DPFPDD_DEV hReader = NULL;
	strncpy(szReader, "", nReaderLen);
	int bStop = 0;
	
	while(!bStop){
		//enumerate the readers
		unsigned int nReaderCnt = 1;
		DPFPDD_DEV_INFO* pReaderInfo = (DPFPDD_DEV_INFO*)malloc(sizeof(DPFPDD_DEV_INFO) * nReaderCnt);
		while(NULL != pReaderInfo){
			unsigned int i = 0;
			for(i = 0; i < nReaderCnt; i++){
				pReaderInfo[i].size = sizeof(DPFPDD_DEV_INFO);
			}
			
			unsigned int nNewReaderCnt = nReaderCnt;
			int result = dpfpdd_query_devices(&nNewReaderCnt, pReaderInfo);
			
			//quit if error
			if(DPFPDD_SUCCESS != result && DPFPDD_E_MORE_DATA != result){
				print_error("dpfpdd_query_devices()", result);
				free(pReaderInfo);
				nReaderCnt = 0;
				break;
			}
			
			//allocate memory if needed and do over
			if(DPFPDD_E_MORE_DATA == result){
				DPFPDD_DEV_INFO* pri = (DPFPDD_DEV_INFO*)realloc(pReaderInfo, sizeof(DPFPDD_DEV_INFO) * nNewReaderCnt);
				if(NULL == pri){
					print_error("realloc()", ENOMEM);
					break;
				}
				pReaderInfo = pri;
				nReaderCnt = nNewReaderCnt;
				continue;
			}
			
			//success
			nReaderCnt = nNewReaderCnt;
			break;
		}
		
		//list readers
		if(0 != nReaderCnt){
			unsigned int nChoice = 0;
			//printf("\nOpen first available reader: %s \n", pReaderInfo[nChoice].name);
			//open reader
			int result = dpfpdd_open(pReaderInfo[nChoice].name, &hReader);
			if(DPFPDD_SUCCESS == result){
				strncpy(szReader, pReaderInfo[nChoice].name, nReaderLen);
			}
			else print_error("dpfpdd_open()", result);
			bStop = 1;
		}
		//else printf("\n\nSearching for readers\n");
		
		
		//release memory
		if(NULL != pReaderInfo) free(pReaderInfo);
		pReaderInfo = NULL;
		nReaderCnt = 0;
	}
	
	return hReader;
}

