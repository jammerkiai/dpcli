/* 
 * command-line tool for identifying 
 * a fingerscan reading
 *  
 */

#include "identify.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "helpers.h"
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>

#include <dpfj.h>

void Identify(DPFPDD_DEV hReader){
	unsigned char* pFeatures1 = NULL;
	unsigned int nFeatures1Size = 0;
	unsigned char* pFeatures2 = NULL;
	unsigned int nFeatures2Size = 0;
	unsigned int matched = 0;
	int bStop = 0;
    DIR* FD;
    FILE* f = NULL;
    struct dirent* in_file;

	while(!bStop){
		logInfo("Ready for identification");
		
		//capture first fingerprint
		int result = CaptureFinger("any finger", hReader, DPFJ_FMD_ISO_19794_2_2005, &pFeatures1, &nFeatures1Size);


		/* Scanning the data directory */
	    if (NULL == (FD = opendir ("/var/local/tmsdata/"))) 
	    {
	        logInfo("Error : Failed to open input directory");
	        bStop = 1;
	        continue;
	    }


	    while ((in_file = readdir(FD))) 
	    {
	        /* On linux/Unix we don't want current and parent directories
	         * If you're on Windows machine remove this two lines
	         */
	        if (!strcmp (in_file->d_name, "."))
	            continue;
	        if (!strcmp (in_file->d_name, ".."))    
	            continue;
	        /* Open directory entry file for common operation */
	        /* TODO : change permissions to meet your need! */

	        char * fullpath = malloc(snprintf(NULL, 0, "/var/local/tmsdata/%s", in_file->d_name) + 1);
			sprintf(fullpath, "/var/local/tmsdata/%s", in_file->d_name);

	        //get reference fingerprint from file
			f = fopen(fullpath, "r");

			if (f == NULL) {
				logInfo("Error : Could not open template file.");
				bStop = 1;
				continue;
			}
			// Determine file size
			fseek(f, 0, SEEK_END);
			nFeatures2Size = ftell(f);
			rewind(f);
			pFeatures2 = (unsigned char*)malloc(nFeatures2Size);
			fread(pFeatures2, sizeof(char), nFeatures2Size, f);

			if(0 == result){
				//run comparison
				unsigned int falsematch_rate = 0;
				result = dpfj_compare(DPFJ_FMD_ISO_19794_2_2005, pFeatures1, nFeatures1Size, 0,
					DPFJ_FMD_ISO_19794_2_2005, pFeatures2, nFeatures2Size, 0, &falsematch_rate);
				
				if(DPFJ_SUCCESS == result){
					const unsigned int target_falsematch_rate = DPFJ_PROBABILITY_ONE / 100000; //target rate is 0.00001
					if(falsematch_rate < target_falsematch_rate){
						char * matchcomment = malloc(snprintf(NULL, 0, "Fingerprint matched : %s", in_file->d_name) + 1);
						sprintf(matchcomment, "Fingerprint matched : %s", in_file->d_name);
						logInfo(matchcomment);
						matched = 1;
						bStop = 1;
						break;
					}
					else{
						//logInfo("Fingerprints did not match.");
						//bStop = 2;
					}
				}
				else print_error("dpfjmx_compare()", result);
				
				if(NULL != pFeatures2) free(pFeatures2);
				pFeatures2 = NULL;
				nFeatures2Size = 0;
				fclose(f);
				f = NULL;
			}
			else bStop = 2;
	    }

	    if (!matched) {
	    	logInfo("No match found");
	    	bStop = 1;
	    }
		//release memory
		if(NULL != pFeatures1) free(pFeatures1);
		pFeatures1 = NULL;
		nFeatures1Size = 0;
		if(NULL != pFeatures2) free(pFeatures2);
		pFeatures2 = NULL;
		nFeatures2Size = 0;
	}
}


