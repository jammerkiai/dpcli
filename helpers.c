/* 
 * Copyright (C) 2011, Digital Persona, Inc.
 *
 * This file is a part of sample code for the UareU SDK 2.x.
 */

#include "helpers.h" 

#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <signal.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// error handling

void print_error(const char* szFunctionName, int nError){
	printf("%s returned ", szFunctionName);
	if(_DP_FACILITY == (unsigned int)(nError >> 16)) printf("DP error %d \n\n", (unsigned int)(nError & 0xffff));
	else printf("system error %d \n\n", (unsigned int)nError);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
// capture

DPFPDD_DEV g_hReader = NULL;

void signal_handler(int nSignal) {
	if(SIGINT == nSignal){
		//cancel capture
		if(NULL != g_hReader) dpfpdd_cancel(g_hReader);
	}
}

int CaptureFinger(const char* szFingerName, DPFPDD_DEV hReader, DPFJ_FMD_FORMAT nFtType, unsigned char** ppFt, unsigned int* pFtSize){
	int result = 0;
	*ppFt = NULL;
	*pFtSize = 0;

	//prepare capture parameters and result
	DPFPDD_CAPTURE_PARAM cparam = {0};
	cparam.size = sizeof(cparam);
	cparam.image_fmt = DPFPDD_IMG_FMT_ISOIEC19794;
	cparam.image_proc = DPFPDD_IMG_PROC_NONE;
	cparam.image_res = 500;
	DPFPDD_CAPTURE_RESULT cresult = {0};
	cresult.size = sizeof(cresult);
	cresult.info.size = sizeof(cresult.info);
	//get size of the image
	unsigned int nImageSize = 0;
	result = dpfpdd_capture(hReader, &cparam, 0, &cresult, &nImageSize, NULL);
	if(DPFPDD_E_MORE_DATA != result){
		print_error("dpfpdd_capture()", result);
		return result;
	}

	unsigned char* pImage = (unsigned char*)malloc(nImageSize);
	if(NULL == pImage){
		print_error("malloc()", ENOMEM);
		return ENOMEM;
	}
		
	//set signal handler
	g_hReader = hReader;
	struct sigaction new_action, old_action;
	new_action.sa_handler = &signal_handler;
	sigemptyset(&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, &old_action);

	//unblock SIGINT (Ctrl-C)
	sigset_t new_sigmask, old_sigmask;
	sigemptyset(&new_sigmask);
	sigaddset(&new_sigmask, SIGINT);
	sigprocmask(SIG_UNBLOCK, &new_sigmask, &old_sigmask);
	
	while(1){
		//capture fingerprint
		//printf("Put %s on the reader, or press Ctrl-C to cancel...\r\n", szFingerName);
		//logInfo("Put your finger on the reader");
		result = dpfpdd_capture(hReader, &cparam, -1, &cresult, &nImageSize, pImage);
		if(DPFPDD_SUCCESS != result){
			print_error("dpfpdd_capture()", result);
		}
		else{
			if(cresult.success){
				//captured
				//logInfo("    fingerprint captured");

				//get max size for the feature template
				unsigned int nFeaturesSize = MAX_FMD_SIZE;
				unsigned char* pFeatures = (unsigned char*)malloc(nFeaturesSize);
				if(NULL == pFeatures){
					print_error("malloc()", ENOMEM);
					result = ENOMEM;
				}
				else{
					//create template
					result = dpfj_create_fmd_from_fid(DPFJ_FID_ISO_19794_4_2005, pImage, nImageSize, nFtType, pFeatures, &nFeaturesSize);

					if(DPFJ_SUCCESS == result){
						*ppFt = pFeatures;
						*pFtSize = nFeaturesSize;
						//logInfo("    features extracted");
					}
					else{
						print_error("dpfj_create_fmd_from_fid()", result);
						free(pFeatures);
					}
				}
			}
			else if(DPFPDD_QUALITY_CANCELED == cresult.quality){
				//capture canceled
				result = EINTR;
			}
			else{
				//bad capture
				logInfo("Bad capture. Try again.");
				continue;
			}
		}
		break;
	}
	
	//restore signal mask
	sigprocmask(SIG_SETMASK, &old_sigmask, NULL);
	
	//restore signal handler
	sigaction (SIGINT, &old_action, NULL);
	g_hReader = NULL;
	
	if(NULL != pImage) free(pImage);
	return result;
}


void logInfo(char* comment) {
	FILE *logfile = fopen("/var/tmp/dpcli.log", "a+");
	fprintf(logfile, "%s\n", comment);
	fclose(logfile);
	printf("%s\n", comment);
}